package heng.chantrea.ams.controller.articlecontroller;

import heng.chantrea.ams.controller.response.BaseApiResponse;
import heng.chantrea.ams.controller.response.NoDataApiResponse;
import heng.chantrea.ams.repository.model.article.Article;
import heng.chantrea.ams.service.ArticleService;
import heng.chantrea.ams.utils.ApiMessaage;
import heng.chantrea.ams.utils.MethodReducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiMessaage.baseUrl)
public class ArticleController {

    private ArticleService articleService;
    private MethodReducer methodReducer;

    @Autowired
    public void setMethodReducer(MethodReducer methodReducer) {
        this.methodReducer = methodReducer;
    }
    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    //TODO: get all article from H2 database
    @GetMapping("/articles")
    public ResponseEntity findAll(){
        BaseApiResponse<List<Article>> baseApiResponse=new BaseApiResponse<>();
        List<Article> article = articleService.findAll();

        if(article.isEmpty()){
            NoDataApiResponse noDataApiResponse = new NoDataApiResponse();
            noDataApiResponse.setMessage(ApiMessaage.NOT_FOUND);
            noDataApiResponse.setStatus(HttpStatus.OK);
            noDataApiResponse.setTime(methodReducer.getTime());
            return ResponseEntity.ok(noDataApiResponse);
        }else{
            baseApiResponse.setData(article);
            baseApiResponse.setStatus(HttpStatus.FOUND);
            baseApiResponse.setTime(methodReducer.getTime());
            baseApiResponse.setMessage(ApiMessaage.GET_SUCCESS);
            return ResponseEntity.ok(baseApiResponse);
        }
    }

    //TODO: get one from H2 database
    @GetMapping("/articles/{id}")
    public ResponseEntity findOne(@PathVariable String id){
        Article articleDto = articleService.findOne(Integer.parseInt(id));
        BaseApiResponse<Article> response = new BaseApiResponse<>();
        response.setMessage(ApiMessaage.GET_ONE_SUCCESS);
        response.setData(articleDto);
        response.setStatus(HttpStatus.OK);response.setTime(methodReducer.getTime());
        return ResponseEntity.ok(response);
    }

    //TODO: search article by category title from H2 database
    @GetMapping("/articles/category")
    public ResponseEntity<BaseApiResponse<List<Article>>> findArticleByCategoryTitle(@RequestParam String search){
        BaseApiResponse<List<Article>> baseApiResponse=new BaseApiResponse<>();
        if (search!=null) {
            List<Article> articleDto = articleService.findArticleByCategoryTitle(search);
            if(articleDto.size()>0){
                baseApiResponse.setData(articleDto);
                baseApiResponse.setStatus(HttpStatus.FOUND);
                baseApiResponse.setTime(methodReducer.getTime());
                baseApiResponse.setMessage(ApiMessaage.GET_SUCCESS);
                return ResponseEntity.ok(baseApiResponse);
            }else{
                baseApiResponse.setMessage(ApiMessaage.NOT_ONE_FOUND);
                baseApiResponse.setStatus(HttpStatus.BAD_REQUEST);
                baseApiResponse.setTime(methodReducer.getTime());
            }
        }
        else{
            baseApiResponse.setMessage(ApiMessaage.NOT_ONE_FOUND);
            baseApiResponse.setStatus(HttpStatus.BAD_REQUEST);
            baseApiResponse.setTime(methodReducer.getTime());
        }
        return ResponseEntity.ok(baseApiResponse);
    }

    //TODO: Post dll data to H2 database
    @PostMapping("/articles")
    public ResponseEntity save(@RequestBody Article article) {
        BaseApiResponse<Article> baseApiResponse=new BaseApiResponse<>();
        Article articles =articleService.save(article);
        baseApiResponse.setMessage(ApiMessaage.INPUT_SUCCESS);
        baseApiResponse.setData(articles);
        baseApiResponse.setStatus(HttpStatus.OK);
        baseApiResponse.setTime(methodReducer.getTime());
        return ResponseEntity.ok(baseApiResponse);
    }

    //TODO: update all article from H2
    @PutMapping("/articles/{id}")
    public ResponseEntity update(@RequestBody Article article,@PathVariable int id) {
        BaseApiResponse<Article> baseApiResponse=new BaseApiResponse<>();
        Article articles =articleService.update(article,id);
        baseApiResponse.setMessage(ApiMessaage.UPDATE_SUCCESS);
        baseApiResponse.setData(articles);
        baseApiResponse.setStatus(HttpStatus.OK);
        baseApiResponse.setTime(methodReducer.getTime());
        return ResponseEntity.ok(baseApiResponse);
    }

    //TODO: delete all article from H2 database
    @DeleteMapping("/articles/{id}")
    public ResponseEntity delete(@PathVariable int id) {
        NoDataApiResponse baseApiResponse = new NoDataApiResponse();
        articleService.delete(id);
        baseApiResponse.setMessage(ApiMessaage.DELETE_SUCCESS);
        baseApiResponse.setStatus(HttpStatus.OK);
        baseApiResponse.setTime(methodReducer.getTime());
        return ResponseEntity.ok(baseApiResponse);
    }
}
