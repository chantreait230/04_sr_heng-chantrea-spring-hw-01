package heng.chantrea.ams.service.article;
import heng.chantrea.ams.repository.ArticleRepository;
import heng.chantrea.ams.repository.model.article.Article;
import heng.chantrea.ams.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {

    private ArticleRepository articleRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public Article findOne(int id) {
        return articleRepository.findOne(id);
    }

    @Override
    public List<Article> findArticleByCategoryTitle(String title) {
        return articleRepository.findArticleByCategoryTitle(title);
    }

    @Override
    public Article save(Article article) {
        articleRepository.save(article);
        entityManager.clear();
        Article article1 = entityManager.find(Article.class,article.getId());
        return article1;
    }

    @Override
    public Article update(Article article,int id) {
        articleRepository.update(article,id);
        entityManager.clear();
        Article article1 = entityManager.find(Article.class,id);
        return article1;
    }

    @Override
    public Article delete(int id) {
        return articleRepository.delete(id);
    }
}
