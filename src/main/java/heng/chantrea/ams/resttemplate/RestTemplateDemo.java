package heng.chantrea.ams.resttemplate;

import heng.chantrea.ams.controller.response.BaseApiResponse;
import heng.chantrea.ams.repository.model.article.Article;
import heng.chantrea.ams.utils.ApiMessaage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;

@RestController
@RequestMapping("/restTemplate")
public class RestTemplateDemo {

    @Autowired
    RestTemplate restTemplate;

    //TODO: get all by using resttemplate
    @GetMapping()
    public String findAll() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> httpEntity = new HttpEntity<String>(httpHeaders);
        return restTemplate.exchange(ApiMessaage.BASE_URL, HttpMethod.GET, httpEntity, String.class).getBody();
    }

    //TODO: get one by using resttemplate
    @GetMapping("/{id}")
    public String findOne(@PathVariable String id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> httpEntity = new HttpEntity<String>(httpHeaders);
        return restTemplate.exchange(ApiMessaage.BASE_URL+"/"+id, HttpMethod.GET, httpEntity, String.class).getBody();
    }

    //TODO: insert article by using resttemplate
    @PostMapping()
    public ResponseEntity<BaseApiResponse> save(@RequestBody Article article){
        HttpEntity<Article> httpEntity = new HttpEntity<>(article);
        ResponseEntity<BaseApiResponse> apiResponseResponseEntity = restTemplate.postForEntity(ApiMessaage.BASE_URL,httpEntity,BaseApiResponse.class);
        return apiResponseResponseEntity;
    }

    //TODO: update article by using resttemplate
    @PutMapping("/{id}")
    public String update(@PathVariable("id") String id, @RequestBody Article article) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<Article> entity = new HttpEntity<Article>(article,headers);
        return restTemplate.exchange(ApiMessaage.BASE_URL+"/"+id, HttpMethod.PUT, entity, String.class).getBody();
    }

    //TODO: delete article by using resttemplate
    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") String id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<Article> entity = new HttpEntity<Article>(headers);
        return restTemplate.exchange(ApiMessaage.BASE_URL+"/"+id, HttpMethod.DELETE, entity, String.class).getBody();
    }


}
