package heng.chantrea.ams.utils;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.regex.Pattern;

@Component
public class MethodReducer {

    public Pattern pattern = Pattern.compile("\\d+");

    public Pattern pattern1=Pattern.compile("[a-zA-Z]");

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss" , timezone = "Asia/Phnom_Penh")
    Timestamp time = new Timestamp(System.currentTimeMillis());

    public ModelMapper getMapper(){
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setAmbiguityIgnored(true);
        mapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT);
        return mapper;
    }


    public Timestamp getTime(){
        return time;
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
}
