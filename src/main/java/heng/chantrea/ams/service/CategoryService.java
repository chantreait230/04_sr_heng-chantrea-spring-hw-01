package heng.chantrea.ams.service;

import heng.chantrea.ams.repository.model.category.Category;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();
    Category save(Category category);
    Category update(Category category,int id);
    Category delete(int id);
}
