DROP TABLE IF EXISTS tb_category;

CREATE TABLE tb_category (
                              id INT AUTO_INCREMENT  PRIMARY KEY,
                              title VARCHAR(250) NOT NULL
);

INSERT  INTO tb_category(id, title) values (null,'spring');