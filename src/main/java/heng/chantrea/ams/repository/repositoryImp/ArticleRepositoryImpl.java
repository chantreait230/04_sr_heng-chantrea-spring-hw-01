package heng.chantrea.ams.repository.repositoryImp;

import heng.chantrea.ams.repository.ArticleRepository;
import heng.chantrea.ams.repository.model.article.Article;
import heng.chantrea.ams.repository.model.category.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ArticleRepositoryImpl implements ArticleRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Article> findAll() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Article> criteriaQuery = criteriaBuilder.createQuery(Article.class);
        Root<Article> root = criteriaQuery.from(Article.class);
        CriteriaQuery<Article> select = criteriaQuery.select(root);
        TypedQuery<Article> typedQuery = entityManager.createQuery(select);
        List<Article> resultList = typedQuery.getResultList();
        return resultList;
    }

    @Override
    public Article findOne(int id) {
        Query query = entityManager.createNamedQuery("findOne").setParameter("id",id);
        return (Article) query.getSingleResult();
    }

    @Override
    public List<Article> findArticleByCategoryTitle(String title) {
        Query query = entityManager.createNamedQuery("findArticleByCategoryTitle").setParameter("title",title);
        return query.getResultList();
    }

    @Override
    public Article save(Article article) {
        entityManager.persist(article);
        return article;
    }

    @Override
    public Article update(Article article,int id) {
        CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
        CriteriaUpdate<Article> update = cb.createCriteriaUpdate(Article.class);
        Root<Article> art = update.from(Article.class);
        update.set("author",article.getAuthor());
        update.set("description",article.getDescription());
        update.set("title",article.getTitle());
        update.set("category",article.getCategory());
        update.where(cb.equal(art.get("id"),id));
        this.entityManager.createQuery(update).executeUpdate();
        return article;
    }

    @Override
    public Article delete(int id) {
        CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
        CriteriaDelete<Article> delete = cb.createCriteriaDelete(Article.class);
        Root<Article> art = delete.from(Article.class);
        delete.where(cb.equal(art.get("id"),id));
        this.entityManager.createQuery(delete).executeUpdate();
        return null;
    }
}
