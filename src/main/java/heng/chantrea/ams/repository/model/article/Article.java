package heng.chantrea.ams.repository.model.article;

import heng.chantrea.ams.repository.model.category.Category;

import javax.persistence.*;

@Entity
@Table(name = "tb_article")
@NamedQuery(name = "findArticleByCategoryTitle",query = "SELECT a FROM Article a Where a.category.title = :title")
@NamedQuery(name = "findOne",query = "SELECT a FROM Article a where a.id=: id")

public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String author;
    private String description;
    private String title;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    public Article() {}

    public Article(int id, String author, String description, String title, Category category) {
        this.id = id;
        this.author = author;
        this.description = description;
        this.title = title;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", author='" + author + '\'' +
                ", description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", category=" + category +
                '}';
    }
}
