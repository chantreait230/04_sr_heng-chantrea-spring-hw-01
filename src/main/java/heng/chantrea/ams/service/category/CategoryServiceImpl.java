package heng.chantrea.ams.service.category;

import heng.chantrea.ams.repository.CategoryRepository;
import heng.chantrea.ams.repository.model.article.Article;
import heng.chantrea.ams.repository.model.category.Category;
import heng.chantrea.ams.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @PersistenceContext
    private EntityManager entityManager;

    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category save(Category category) {
        categoryRepository.save(category);
        return  category;
    }

    @Override
    public Category update(Category category, int id) {
        categoryRepository.update(category,id);
        entityManager.clear();
        Category category1 = entityManager.find(Category.class,id);
        return category1;
    }

    @Override
    public Category delete(int id) {
        return categoryRepository.delete(id);
    }
}
