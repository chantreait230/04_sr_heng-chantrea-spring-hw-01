package heng.chantrea.ams.repository;

import heng.chantrea.ams.repository.model.category.Category;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface CategoryRepository {
    List<Category> findAll();
    Category save(Category category);
    Category update(Category category,int id);
    Category delete(int id);
}
