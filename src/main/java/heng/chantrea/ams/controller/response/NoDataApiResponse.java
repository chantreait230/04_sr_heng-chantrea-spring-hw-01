package heng.chantrea.ams.controller.response;

import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

public class NoDataApiResponse {
    private String message;
    private HttpStatus status;
    private Timestamp time;

    public NoDataApiResponse() {
    }

    public NoDataApiResponse(String message, HttpStatus status, Timestamp time) {
        this.message = message;
        this.status = status;
        this.time = time;
    }

    @Override
    public String toString() {
        return "FailedApiResponse{" +
                "message='" + message + '\'' +
                ", status=" + status +
                ", time=" + time +
                '}';
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
