package heng.chantrea.ams.repository.repositoryImp;

import heng.chantrea.ams.repository.CategoryRepository;
import heng.chantrea.ams.repository.model.article.Article;
import heng.chantrea.ams.repository.model.category.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CategoryRepositoryImpl implements CategoryRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Category> findAll() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Category> criteriaQuery = criteriaBuilder.createQuery(Category.class);
        Root<Category> root = criteriaQuery.from(Category.class);
        CriteriaQuery<Category> select = criteriaQuery.select(root);
        TypedQuery<Category> typedQuery = entityManager.createQuery(select);
        List<Category> resultList = typedQuery.getResultList();
        return resultList;
    }

    @Override
    public Category save(Category category) {
        entityManager.persist(category);
        return category;
    }

    @Override
    public Category update(Category category, int id) {
        CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
        CriteriaUpdate<Category> update = cb.createCriteriaUpdate(Category.class);
        Root<Category> cate = update.from(Category.class);
        update.set("title",category.getTitle());
        update.where(cb.equal(cate.get("id"),id));
        this.entityManager.createQuery(update).executeUpdate();
        return category;
    }

    @Override
    public Category delete(int id) {
        CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
        CriteriaDelete<Category> delete = cb.createCriteriaDelete(Category.class);
        Root<Category> cate = delete.from(Category.class);
        delete.where(cb.equal(cate.get("id"),id));
        this.entityManager.createQuery(delete).executeUpdate();
        return null;
    }

}
