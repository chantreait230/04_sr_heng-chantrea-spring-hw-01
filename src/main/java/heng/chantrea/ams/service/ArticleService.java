package heng.chantrea.ams.service;


import heng.chantrea.ams.repository.model.article.Article;

import java.util.List;

public interface ArticleService {
    List<Article> findAll();
    Article findOne(int id);
    List<Article> findArticleByCategoryTitle(String title);
    Article save(Article article);
    Article update(Article article,int id);
    Article delete(int id);
}
