package heng.chantrea.ams.controller.categorycontroller;

import com.fasterxml.jackson.annotation.JsonFormat;
import heng.chantrea.ams.controller.response.BaseApiResponse;
import heng.chantrea.ams.controller.response.NoDataApiResponse;
import heng.chantrea.ams.repository.model.article.Article;
import heng.chantrea.ams.repository.model.category.Category;
import heng.chantrea.ams.service.CategoryService;
import heng.chantrea.ams.utils.ApiMessaage;
import heng.chantrea.ams.utils.MethodReducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping(ApiMessaage.baseUrl)
public class CategoryController {

    private CategoryService categoryService;
    private MethodReducer methodReducer;

    @Autowired
    public void setMethodReducer(MethodReducer methodReducer) {
        this.methodReducer = methodReducer;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    //TODO: get all article from H2 database
    @GetMapping("/categories")
    public ResponseEntity findAll(){
            BaseApiResponse<List<Category>> baseApiResponse=new BaseApiResponse<>();
            List<Category> category =categoryService.findAll();

            if(category.isEmpty()){
                NoDataApiResponse noDataApiResponse=new NoDataApiResponse();
                noDataApiResponse.setMessage(ApiMessaage.NOT_FOUND);
                noDataApiResponse.setStatus(HttpStatus.OK);
                noDataApiResponse.setTime(methodReducer.getTime());
                return ResponseEntity.ok(noDataApiResponse);
            }else{
                baseApiResponse.setData(category);
                baseApiResponse.setStatus(HttpStatus.FOUND);
                baseApiResponse.setTime(methodReducer.getTime());
                baseApiResponse.setMessage(ApiMessaage.GET_SUCCESS);
                return ResponseEntity.ok(baseApiResponse);
            }

    }

    //TODO: post article from H2 database
    @PostMapping("/categories")
    public ResponseEntity<Category> save(@RequestBody Category category){
        categoryService.save(category);
        return new ResponseEntity<>(categoryService.save(category), HttpStatus.OK);
    }

    //TODO: update from H2 database
    @PutMapping("/categories/{id}")
    public ResponseEntity update(@RequestBody Category category, @PathVariable int id) {
        BaseApiResponse<Category> baseApiResponse=new BaseApiResponse<>();
        Category category1 = categoryService.update(category,id);
        baseApiResponse.setMessage(ApiMessaage.UPDATE_SUCCESS);
        baseApiResponse.setData(category1);
        baseApiResponse.setStatus(HttpStatus.OK);
        baseApiResponse.setTime(methodReducer.getTime());
        return ResponseEntity.ok(baseApiResponse);
    }

    //TODO: TO delete all article from H2 database
    @DeleteMapping("/categories/{id}")
    public ResponseEntity delete(@PathVariable int id) {
        NoDataApiResponse baseApiResponse=new NoDataApiResponse();
        categoryService.delete(id);
        baseApiResponse.setMessage(ApiMessaage.DELETE_SUCCESS);
        baseApiResponse.setStatus(HttpStatus.OK);
        baseApiResponse.setTime(methodReducer.getTime());
        return ResponseEntity.ok(baseApiResponse);
    }

}
